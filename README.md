Build a DevOps pipeline with GitLab in the ASE course

## Project Badges

What's the state of the pipeline?
[![pipeline status](https://gitlab.com/carminevassallo/spring-boot-initial/badges/main/pipeline.svg)](https://gitlab.com/carminevassallo/spring-boot-initial/-/commits/main)

How many bugs in my code?
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=carminevassallo_spring-boot-initial&metric=bugs)](https://sonarcloud.io/dashboard?id=carminevassallo_spring-boot-initial)

How many code smells did I introduce?
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=carminevassallo_spring-boot-initial&metric=code_smells)](https://sonarcloud.io/dashboard?id=carminevassallo_spring-boot-initial)

Is quality of my code enough to release my application?
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=carminevassallo_spring-boot-initial&metric=alert_status)](https://sonarcloud.io/dashboard?id=carminevassallo_spring-boot-initial)